# What is Blackthorn?

Blackthorn is a framework for writing 2D games in Common Lisp.
Blackthorn is attempt to write an efficient, dynamic, persistent 2D game
engine in an expressive language which makes it easy to write games.

# Why another game engine?

Games are hard to write. The effort needed to write a usable game engine
from scratch, especially when dealing with the low-level details of
languages like C, make the cost of writing games prohibitive. Libraries
like [SDL](http://www.libsdl.org/) get many of the driver-level graphics
details out of the way, but still leave the user writing in C. Libraries
like [PyGame](http://www.pygame.org/news.html) and
[LISPBUILDER-SDL](http://code.google.com/p/lispbuilder/wiki/LispbuilderSDL)
wrap more of these low-level details, but still don’t provide a full
game engine needed for writing substantial games.

There are, of course, game engines which provide this functionality to
the user. [Game Maker](http://www.yoyogames.com/gamemaker/), for
example, is an engine which provides everything needed to make a basic
game, and an extention language for writing more complex behavior. Using
Game Maker, an experienced user can write a basic game in five minutes.
However, Game Maker (and similar programs the authors have tried) have
some substantial flaws. Problems with Game Maker, specifically, include:

-   Game Maker only runs on Windows. A Linux port is still a dream, and
    porting to any sort of mobile device is completely unimaginable.

-   Game Maker’s extension language, GML, is a kludge, and inefficient.
    (The lack of a rich set of built-in datastructures is something I
    hear GML users complaining about frequently.)

-   Game Maker is closed source, so it would be impossible for anyone
    other than the authors to fix any of the above problems with Game
    Maker.

# What does Blackthorn provide?

Blackthorn attempts to fix many of the problems above. Blackthorn
provides:

-   A not-yet-complete subset of the functionality provided by Game
    Maker. Despite being incomplete, Blackthorn is already capable of
    supporting simple games.

-   Blackthorn is written in Common Lisp, providing:

    -   Efficiency which is (depending on the implementation, and the
        benchmark) capable of competing with C.

    -   Portable to any platform supported by a compliant ANSI Common
        Lisp compiler. Blackthorn currently runs on Windows, Linux, and
        Mac OSX. Porting Blackthorn to a new compiler takes a couple of
        hours.

    -   Dynamic behavior, because the entire compiler is available at
        runtime. An on-screen development REPL (read-eval-print loop,
        i.e. a development console) with an on-screen debugger is
        provided, giving the user the ability to rewrite arbitrary
        pieces of code on the fly.

    -   Extensibility, because the game engine itself is an open
        platform, and because user code operates at the same level as
        the game engine.

    -   And finally, because Blackthorn is open source, it is open to
        improvements from the community.

# Technical details

Blackthorn uses [LISPBUILDER-SDL](http://code.google.com/p/lispbuilder/)
for graphics support (which internally uses
[SDL](http://www.libsdl.org/) and
[SDL\_image](http://www.libsdl.org/projects/SDL_image/)), and
[CL-STORE](http://common-lisp.net/project/cl-store/) as an internal
database for object persistence.

Blackthorn currently runs on Windows, Linux, and Mac OS X, under
[Allegro CL](http://franz.com/products/allegrocl/),
[CLISP](http://clisp.cons.org/), [Clozure
CL](http://trac.clozure.com/openmcl), and [SBCL](http://www.sbcl.org/).
Blackthorn has been tested successfully on the following OS/Lisp
combinations:

  ------------ --------- ------- ----------
                Windows   Linux   Mac OS X
   Allegro CL     Yes      ??        ??
     CLISP        Yes      Yes      Yes
   Clozure CL     Yes      Yes       No
      SBCL        Yes      Yes      Yes
  ------------ --------- ------- ----------

Among the compatible compilers, SBCL is suggested because it is (a) free
and open source, (b) compatible with Windows, Linux and Mac, and (c) has
the best performance of the compilers listed. Allegro CL is also a good
choice, but is commercial software (although a free version is
available).

## Direct dependencies

-   [LISPBUILDER-SDL
    and -SDL-IMAGE](http://code.google.com/p/lispbuilder/)

-   [CL-STORE](http://common-lisp.net/project/cl-store/)

-   [CL-Containers](http://www.common-lisp.org/project/cl-containers/)

-   [Trivial Garbage](http://www.cliki.net/trivial-garbage)

-   [CL-FAD](http://www.weitz.de/cl-fad/)

## Windows only (optional)

-   [Cygwin](http://www.cygwin.com/) or
    [GnuWin32](http://gnuwin32.sourceforge.net/) to use the Makefile

-   [NSIS](http://nsis.sourceforge.net/) for building installers

# Installation

Download the source using darcs

    darcs get http://common-lisp.net/~eslaughter/darcs/blackthorn

To start Blackthorn from the shell, merely call make

    make

Optionally, use parameters to specify the build environment, e.g.

    make cl=sbcl db=nodb driver=load.lisp system=bunnyslayer

If instead you prefer to start Blackthorn interactively, start your Lisp
and

    (load "load")

# Download

Binary distributions are made semi-frequently and are available for
download at <http://elliottslaughter.net/bunnyslayer/download>. License

Blackthorn is free and open source software, see the
[COPYRIGHT](http://common-lisp.net/~eslaughter/darcs/blackthorn/COPYRIGHT)
file for details.
