**Please note:** Blackthorn is no longer under active development, and
the code is mostly of historical interest only.

Blackthorn is a 2D game engine written in Common Lisp. See the
[wiki](https://bitbucket.org/elliottslaughter/blackthorn-engine/wiki/Blackthorn)
for more information.

So far, two games have been written using Blackthorn:

  * [Thopter](https://bitbucket.org/elliottslaughter/blackthorn-engine/wiki/Thopter) (latest version 0.4.5 as of 11/06/2010)
      * Scrolling shooter game in the style of Raptor: Call of the Shadows
      * LAN multiplayer co-op support
      * Videos of Thopter: [ v0.3.2](http://www.youtube.com/watch?v=jqAp-1EZoFg v0.4.1] [http://www.youtube.com/watch?v=xfOnCHauYTM) [v0.0](http://www.youtube.com/watch?v=5MDqC6XG-Ag)
      * Downloads: [Windows](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DThopter+OpSys%3DWindows) [Mac](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DThopter+OpSys%3DOSX) [Linux](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DThopter+OpSys%3DLinux)
  * [Bunny Slayer](https://bitbucket.org/elliottslaughter/blackthorn-engine/wiki/BunnySlayer) (latest version 0.1+76 as of 06/07/2009)
      * 2D top-down RPG inspired by Zelda: A Link to the Past et al
      * Downloads: [Windows](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DBunnySlayer+OpSys%3DWindows) [Mac](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DBunnySlayer+OpSys%3DOSX) [Linux](http://code.google.com/p/blackthorn-engine/downloads/list?can=2&q=Component%3DBunnySlayer+OpSys%3DLinux)

See the
[wiki](https://bitbucket.org/elliottslaughter/blackthorn-engine/wiki/Blackthorn)
for design rationale, feature lists, documentation, installation
guides, and more.

The Blackthorn API is of roughly alpha quality. It is stable in the
sense that full games have been built on top of it, but is missing
some features one might expect from a quality game engine.

There is also [Blackthorn 3D](http://code.google.com/p/blackthorn-engine-3d/),
a 3D game engine written in Common Lisp. The APIs for the two
libraries do not overlap; if you want 2D use Blackthorn 2D, if you
want 3D use Blackthorn 3D. At this time there is no plan to integrate
them.

If you would like to use Blackthorn for a project, please let me know!
